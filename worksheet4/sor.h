#ifndef __SOR_H_
#define __SOR_H_

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "parallel.h"

/**
 * One GS iteration for the pressure Poisson equation. Besides, the routine must 
 * also set the boundary values for P according to the specification. The 
 * residual for the termination criteria has to be stored in res.
 * 
 * An \omega = 1 GS - implementation is given within sor.c.
 */
void sor(
  double omg,
  double dx,
  double dy,
  int    i_lower,
  int    i_upper,
  int	 j_lower,
  int 	 j_upper,
  double **P,
  double **RS,
  double *res,
  int imax,
  int jmax,
  int NeighborID_bottom,
  int NeighborID_top,
  int NeighborID_li,
  int NeighborID_re,
  int myrank,
  MPI_Status *status,
  double *send_buf,
  double *recv_buf

);


#endif
