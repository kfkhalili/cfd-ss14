#include "parallel.h"


void Program_Message(char *txt)
/* produces a stderr text output  */

{
   int myrank;

   MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
   fprintf(stderr,"-MESSAGE- P:%2d : %s\n",myrank,txt);
   fflush(stdout);
   fflush(stderr);
}


void Programm_Sync(char *txt)
/* produces a stderr textoutput and synchronize all processes */

{
   int myrank;

   MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
   MPI_Barrier(MPI_COMM_WORLD);                             /* synchronize output */  
   fprintf(stderr,"-MESSAGE- P:%2d : %s\n",myrank,txt);
   fflush(stdout);
   fflush(stderr);
   MPI_Barrier(MPI_COMM_WORLD);
}


void Programm_Stop(char *txt)
/* all processes will produce a text output, be synchronized and finished */

{
   int myrank;

   MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
   MPI_Barrier(MPI_COMM_WORLD);                           /* synchronize output */
   fprintf(stderr,"-STOP- P:%2d : %s\n",myrank,txt);
   fflush(stdout);
   fflush(stderr);
   MPI_Barrier(MPI_COMM_WORLD);
   MPI_Finalize();
   exit(1);
}


void parallel_init(int myrank, int iproc, int jproc, int imax, int jmax, int *myrow, int *mycol, int *NeighborID_bottom, int *NeighborID_top,
		int *NeighborID_li, int *NeighborID_re, int *i_lower, int *i_upper, int *j_lower, int *j_upper)
{

    int i_quotient, i_remainder, j_quotient, j_remainder;
	/** Domain Decomposition ***/
    *myrow=myrank/iproc;
    *mycol=myrank % iproc;

    if ( *myrow == 0 )           { *NeighborID_bottom = MPI_PROC_NULL; }
    else { *NeighborID_bottom = myrank - iproc; }

    if ( *myrow == jproc - 1 )   { *NeighborID_top = MPI_PROC_NULL;    }
    else { *NeighborID_top = myrank + iproc; }

    if (*mycol == 0 )            { *NeighborID_li    = MPI_PROC_NULL; }
    else { *NeighborID_li = myrank - 1; }

    if (*mycol == iproc - 1 )    { *NeighborID_re    = MPI_PROC_NULL; }
    else { *NeighborID_re = myrank + 1; }

    /*
    printf("\n\nProc ID:%d  Row:%d Col:%d right:%d left:%d top%d bottom%d\n\n", myrank, myrow, mycol,NeighborID_re, NeighborID_li, NeighborID_top, NeighborID_bottom );
*/

    i_quotient  = imax/iproc;
    i_remainder = imax% iproc;

    j_quotient  = jmax/jproc;
    j_remainder = jmax%jproc;

    *i_lower     = *mycol       * i_quotient + min(*mycol     , i_remainder) + 1;
    *i_upper     = (*mycol + 1) * i_quotient + min(*mycol + 1 , i_remainder);

    *j_lower     = *myrow       * j_quotient + min(*myrow     , j_remainder) + 1;
    *j_upper     = (*myrow + 1) * j_quotient + min(*myrow + 1 , j_remainder);

}

void pressure_comm(double **P, int i_upper, int i_lower, int j_lower, int j_upper,
		int NeighborID_li, int NeighborID_re, int NeighborID_bottom, int NeighborID_top,  MPI_Status *status,
		double *send_buf, double *recv_buf)
{

	int i,j;
	send_buf = malloc( ( j_upper - j_lower + 1 ) *sizeof(*send_buf) );
	recv_buf = malloc( ( j_upper - j_lower + 1 ) *sizeof(*recv_buf) );


    /* send left, receive right */
	if(NeighborID_li != MPI_PROC_NULL)
	{
		for (j = j_lower; j <= j_upper; j++)
		{
			send_buf[j - j_lower] = P[i_lower][j];
		}
	}

	MPI_Sendrecv(send_buf, j_upper - j_lower + 1, MPI_DOUBLE,
				  NeighborID_li, 1,
				  recv_buf,  j_upper - j_lower + 1, MPI_DOUBLE,
				  NeighborID_re, 1,
				  MPI_COMM_WORLD, status);

	/* setting right boundary with received vals */
	if(NeighborID_re != MPI_PROC_NULL)
	{
		for (j = j_lower; j <= j_upper; j++)
		{
			P[i_upper + 1][j]=recv_buf[j-j_lower];
		}
	}
/*******************************************************/
	/* sending right, receive left */
	if(NeighborID_re != MPI_PROC_NULL)
	{
		for (j = j_lower; j <= j_upper; j++)
		{
			send_buf[j-j_lower] = P[i_upper][j];
		}
	}

	MPI_Sendrecv(send_buf, j_upper - j_lower + 1, MPI_DOUBLE,
				  NeighborID_re, 2,
				  recv_buf,  j_upper - j_lower + 1, MPI_DOUBLE,
				  NeighborID_li, 2,
				  MPI_COMM_WORLD, status);

	/* setting left boundary with received values */

	if(NeighborID_li != MPI_PROC_NULL)
	{
		for (j = j_lower; j <= j_upper; j++)
		{
			P[i_lower - 1][j] = recv_buf[j-j_lower];
		}
	}

	free(send_buf);
	free(recv_buf);
/*************************************************************************/
	send_buf = malloc( ( i_upper - i_lower + 1 ) * sizeof(*send_buf) );
	recv_buf = malloc( ( i_upper - i_lower + 1 ) * sizeof(*recv_buf) );


	/* send top, receive bottom */
	if(NeighborID_top != MPI_PROC_NULL)
	{
		for (i = i_lower; i <= i_upper; i++)
		{
			send_buf[i - i_lower] = P[i][j_upper];
		}
	}
	MPI_Sendrecv( send_buf, i_upper - i_lower + 1, MPI_DOUBLE,
	              NeighborID_top, 3,
	              recv_buf, i_upper - i_lower + 1, MPI_DOUBLE,
	              NeighborID_bottom, 3,
	              MPI_COMM_WORLD, status);

	/* setting bottom boundary with the new values */
	if( NeighborID_bottom != MPI_PROC_NULL)
	{
		for (i = i_lower; i <= i_upper; i++)
		{
			P[i][j_lower - 1] = recv_buf[i - i_lower];
		}
	}

    /* send bottom, receive top */
	if( NeighborID_bottom != MPI_PROC_NULL)
	{
		for (i = i_lower; i <= i_upper; i++)
		{
			send_buf[i - i_lower] = P[i][j_lower];
		}
	}

	MPI_Sendrecv( send_buf, i_upper - i_lower + 1, MPI_DOUBLE,
	              NeighborID_bottom, 4,
	              recv_buf, i_upper - i_lower + 1, MPI_DOUBLE,
	              NeighborID_top, 4,
	              MPI_COMM_WORLD, status);

	/* setting top boundary with the new values */
	if( NeighborID_top != MPI_PROC_NULL)
	{
		for (i = i_lower; i <= i_upper; i++)
		{
			P[i][j_upper + 1]= recv_buf[i - i_lower];
		}
	}

	free(send_buf);
	free(recv_buf);

}


void uv_comm(double **U, double **V, int i_upper, int i_lower, int j_lower, int j_upper,
		int NeighborID_li, int NeighborID_re, int NeighborID_bottom, int NeighborID_top,  MPI_Status *status,
		double *send_buf, double *recv_buf)
{

	int i,j;
	int U_size, V_size , UV_size;

	U_size = i_upper - i_lower + 2;
	V_size = i_upper - i_lower + 1;
	UV_size = U_size + V_size;

	send_buf = malloc( UV_size * sizeof(*send_buf));
	recv_buf = malloc( UV_size * sizeof(*recv_buf));

	/** ********/
	if (NeighborID_top != MPI_PROC_NULL)
	{
		for ( i =0; i < V_size; i++ )
		{
			send_buf[i] = V[i_lower + i][j_upper -1];
		}
		for ( i =0; i < U_size; i++ )
		{
			send_buf[i + V_size] = U[i_lower - 1 + i][j_upper];
		}
	}

	 MPI_Sendrecv(send_buf, UV_size, MPI_DOUBLE,
	              NeighborID_top, 5,
	              recv_buf, UV_size, MPI_DOUBLE,
	              NeighborID_bottom, 5,
	              MPI_COMM_WORLD, status);

	 if ( NeighborID_bottom != MPI_PROC_NULL)
	 {
			for (i =0; i < V_size; i++ )
			{
				V[i_lower + i][j_lower - 2 ] 	= recv_buf[i];
			}
			for (i =0; i < U_size; i++ )
			{
				U[i_lower - 1 + i][j_lower - 1] = recv_buf[i + V_size];
			}
	 }
	 /****************/
	if (NeighborID_bottom != MPI_PROC_NULL)
	{
		for ( i = 0; i < V_size; i++ )
		{
			send_buf[i] = V[i_lower + i][j_lower];
		}
		for (i = 0; i < U_size; i++ )
		{
			send_buf[i + V_size] = U[i_lower - 1 + i][j_lower];
		}
	}

	 MPI_Sendrecv(send_buf, UV_size, MPI_DOUBLE,
				  NeighborID_bottom, 6,
				  recv_buf, UV_size, MPI_DOUBLE,
				  NeighborID_top, 6,
				  MPI_COMM_WORLD, status);

	 if ( NeighborID_top != MPI_PROC_NULL)
	 {
			for ( i =0; i < V_size; i++ )
			{
				V[i_lower + i][j_upper + 1] = recv_buf[i];
			}
			for (i =0; i < U_size; i++ )
			{
				U[i_lower - 1 + i][j_upper + 1] = recv_buf[i + V_size];
			}
	 }

	 free(send_buf);
	 free(recv_buf);

	 /**********************************/

	 U_size = j_upper - j_lower + 1;
	 V_size = j_upper - j_lower + 2;
	 UV_size = U_size + V_size;

	 send_buf = malloc( UV_size * sizeof(*send_buf));
	 recv_buf = malloc( UV_size * sizeof(*recv_buf));

	 /* send left, receive right */
	if (NeighborID_li != MPI_PROC_NULL)
	{
		for (j =0; j < V_size; j++ )
		{
			send_buf[j] = V[i_lower][j_lower - 1 + j];
		}
		for (j =0; j < U_size; j++ )
		{

			send_buf[j + V_size] = U[i_lower][j_lower + j];
		}
	}

	 MPI_Sendrecv(send_buf, UV_size, MPI_DOUBLE,
				  NeighborID_li, 7,
				  recv_buf, UV_size, MPI_DOUBLE,
				  NeighborID_re, 7,
				  MPI_COMM_WORLD, status);

	 if ( NeighborID_re != MPI_PROC_NULL)
	 {
			for ( j =0; j < V_size; j++ )
			{
				V[i_upper + 1][j_lower + j - 1] = recv_buf[j];
			}
			for (j =0; j < U_size; j++ )
			{
				U[i_upper + 1 ][j_lower + j] = recv_buf[j + V_size];
			}
	 }

	 /* send right, receive left */
	if (NeighborID_re != MPI_PROC_NULL)
	{
		for (j =0; j < V_size; j++ )
		{
			send_buf[j] = V[i_upper][j_lower - 1 + j];
		}
		for (j =0; j < U_size; j++ )
		{
			send_buf[j + V_size] = U[i_upper - 1][j_lower + j];
		}
	}

	 MPI_Sendrecv(send_buf, UV_size, MPI_DOUBLE,
				  NeighborID_re, 8,
				  recv_buf, UV_size, MPI_DOUBLE,
				  NeighborID_li, 8,
				  MPI_COMM_WORLD, status);

	 if ( NeighborID_li != MPI_PROC_NULL)
	 {
			for ( j =0; j < V_size; j++ )
			{
				V[i_lower - 1][j_lower + j - 1] = recv_buf[j];
			}
			for (j =0; j < U_size; j++ )
			{
				U[i_lower - 2 ][j_lower + j] = recv_buf[j + V_size];
			}
	 }

	 free(send_buf);
	 free(recv_buf);
}
