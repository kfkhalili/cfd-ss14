#include "streaming.h"
#include "LBDefinitions.h"

void doStreaming(double *collideField, double *streamField,int *flagField,int xlength)
{

    int dim = xlength +2;
    int xpos = 19;
    int ypos = 19*dim;
    int zpos = 19*dim*dim;

    for (int z = 0; z < dim ; z++)
    {
        for (int y = 0; y < dim ; y++)
        {
            for (int x = 0; x < dim ; x++)
            {
                int localindex = x + dim*y + dim*dim*z;
                /** loops through the matrix taking into account of matrix
                dimension including boundary if xlength+2 **/

                int cell_index = 19*localindex;
                /**** We consider that each cell stores 19 Fi *******/

                /*** Only for fluid cells do streaming  **/
                if (flagField[ localindex ] == 0)
                {

                    for (int q=0; q<19; q++){
                    streamField[ cell_index + q ] = collideField[ cell_index - xpos*LATTICEVELOCITIES[q][0]
                    - ypos* LATTICEVELOCITIES[q][1] - zpos* LATTICEVELOCITIES[q][2] +q ];
                    }

                }
            }
        }
    }
}

