#include "helper.h"
#include "visual.h"
#include "init.h"
#include "uvp.h"
#include "boundary_val.h"
#include "sor.h"
#include <stdio.h>


/**
 * The main operation reads the configuration file, initializes the scenario and
 * contains the main loop. So here are the individual steps of the algorithm:
 *
 * - read the program configuration file using read_parameters()
 * - set up the matrices (arrays) needed using the matrix() command
 * - create the initial setup init_uvp(), init_flag(), output_uvp()
 * - perform the main loop
 * - trailer: destroy memory allocated and do some statistics
 *
 * The layout of the grid is decribed by the first figure below, the enumeration
 * of the whole grid is given by the second figure. All the unknowns corresond
 * to a two dimensional degree of freedom layout, so they are not stored in
 * arrays, but in a matrix.
 *
 * @image html grid.jpg
 *
 * @image html whole-grid.jpg
 *
 * Within the main loop the following big steps are done (for some of the
 * operations a definition is defined already within uvp.h):
 *
 * - calculate_dt() Determine the maximal time step size.
 * - boundaryvalues() Set the boundary values for the next time step.
 * - calculate_fg() Determine the values of F and G (diffusion and confection).
 *   This is the right hand side of the pressure equation and used later on for
 *   the time step transition.
 * - calculate_rs()
 * - Iterate the pressure poisson equation until the residual becomes smaller
 *   than eps or the maximal number of iterations is performed. Within the
 *   iteration loop the operation sor() is used.
 * - calculate_uv() Calculate the velocity at the next time step.
 */
int main(int argn, char** args)
{
    double Re;
    double UI;
    double VI;
    double PI;
    double GX;
    double GY;
    double t_end;
    double xlength;
    double ylength;
    double dt;
    double dx;
    double dy;
    int  imax;
    int  jmax;
    double alpha;
    double omg;
    double tau;
    int  itermax;
    double eps;
    double dt_value;
    double t;
    int it;
    int n;
    double res;
    const char *szProblem = "cavity100";
    double **    U = NULL;
    double **    V = NULL;
    double **    P = NULL;
    double **    P_old = NULL;

    double **    F  = NULL;
    double **    G  = NULL;
    double **    RS = NULL;

    /******* Read parameters from file ***********/
    read_parameters( "cavity100.dat", &Re,&UI,&VI,&PI,&GX,&GY,&t_end,&xlength,&ylength,
                    &dt,&dx,&dy,&imax,&jmax,&alpha,&omg,&tau,&itermax,&eps,&dt_value);

    /********* Allocation of memory for U, V, P, F, G, RS **************/
    U = matrix(0,(imax+1),0,(jmax+1));
    V = matrix(0,(imax+1),0,(jmax+1));
    P = matrix(0,(imax+1),0,(jmax+1));
    P_old = matrix(0,(imax+1),0,(jmax+1));

    F  = matrix(0,imax,0,jmax);
    G  = matrix(0,imax,0,jmax);
    RS = matrix(0,imax,0,jmax);


    t = 0;      /**     Initialisation of time                      **/
    n = 0;      /**     Initial time step iteration number = 0      **/

    /********** Initialising the matrices U,V,P in the inner points to initial values UI,VI,PI **********/
    init_uvp(UI,VI,PI,imax,jmax,U,V,P,P_old);

   while(t < t_end)
    {
        /******** Calculation of time step per iteration for stability, CFL number calculations*******/
        calculate_dt(Re,tau, &dt,dx,dy,imax,jmax,U,V);

        /*** Implementation of boundary condition, no slip for all walls and upper wall is a moving wall *****/
        boundaryvalues(imax,jmax,U,V);

        /******** Computation of the matrix F & G ***/
        calculate_fg(Re,GX,GY,alpha,dt,dx,dy,imax,jmax,U,V,F,G );

        /********  Computation of RS of LSE for P computation ******/
        calculate_rs(dt,dx,dy,imax,jmax,F,G,RS);

        it=0;
        res = 10;

        /********  SOR method for solving P  *****/
        while( it<itermax && fabs(res) > eps)
        {
            sor(omg,dx,dy,imax,jmax,P,P_old,RS,&res);
            it=it+1;
        }

        /***  Print a warning if SOR didnt converge in itermax iterations
        ****  (Maximum iterations specified by user in cavity100.dat) **********/
        /***   Also check for residual explosion  ***********/        
        if (it == itermax)
        {
            printf("The iterations did not converge \n");
        }

        if (res != res)
        {
            printf("INSTABILITY -- Residual is infinite !! \n");
        }
        

        /****  Computation of U, V of next time step based on F,G,P computed and U,V values of current time step**/
        calculate_uv(dt,dx,dy,imax,jmax,U,V,F,G,P);

        n++;        /** Increement the time step iteration number **/
        t= t+dt;    /** Compute of current total time by increementing the time with the time step per iteration **/

        /**** Print Time step, Time step iteration, SOR iteration and SOR residual to the screen*****/
        printf("dt = %f, Time step iteration=%u, SOR iterations=%u, SOR res=%f \n\n",dt,n,it,res);
    }

    /****** Write an output vtk file for visualisation at t = t_end *******/
    write_vtkFile(szProblem,t,xlength,ylength,imax,jmax,dx,dy,U,V,P);

    /****** Free allocated memory for U, V, P, F and G  ******/    
    free_matrix(U,0,(imax+1),0,(jmax+1));
    free_matrix(V,0,(imax+1),0,(jmax+1));
    free_matrix(P,0,(imax+1),0,(jmax+1));
    free_matrix(P_old, 0, (imax+1), 0, (jmax+1));

    free_matrix(F,0,imax,0,jmax);
    free_matrix(G,0,imax,0,jmax);
    free_matrix(RS,0,imax,0,jmax);

  return -1;
}
