#include "sor.h"
#include <math.h>


void sor(
  double omg,
  double dx,
  double dy,
  int    i_lower,
  int    i_upper,
  int	 j_lower,
  int 	 j_upper,
  double **P,
  double **RS,
  double *res,
  int imax,
  int jmax,
  int NeighborID_bottom,
  int NeighborID_top,
  int NeighborID_li,
  int NeighborID_re,
  int myrank,
  MPI_Status *status,
  double *send_buf,
  double *recv_buf
) {

  int i,j;
  double rloc;
  double coeff = omg/(2.0*(1.0/(dx*dx)+1.0/(dy*dy)));
  double *send_bottom;
  double *send_top;
  double *send_right;
  double *send_left;
  double *recv_bottom;
  double *recv_top;
  double *recv_right;
  double *recv_left;

  /* set boundary values */

 if ( j_lower == 1)
 {
	  for(i = i_lower -1 ; i <= i_upper +1; i++) {
		P[i][0] = P[i][1];
	  }
 }
 if ( j_upper == jmax)
 {
	  for(i = i_lower - 1; i <= i_upper +1; i++) {
		P[i][jmax+1] = P[i][jmax];
	  }
 }

if ( i_lower == 1)
{
  for(j = j_lower - 1; j <= j_upper +1; j++) {
    P[0][j] = P[1][j];
  }
}
if ( i_upper == imax)
{
  for(j = j_lower -1; j <= j_upper +1; j++) {
    P[imax+1][j] = P[imax][j];
  }
}


  /* SOR iteration */
  for(i = i_lower; i <= i_upper; i++) {
    for(j = j_lower; j <= j_upper; j++) {
      P[i][j] = (1.0-omg)*P[i][j]
              + coeff*(( P[i+1][j]+P[i-1][j])/(dx*dx) + ( P[i][j+1]+P[i][j-1])/(dy*dy) - RS[i][j]);
    }
  }

/** communicate pressure and then compute residual **/
  pressure_comm(P, i_upper,i_lower,j_lower, j_upper,
  		NeighborID_li, NeighborID_re, NeighborID_bottom, NeighborID_top, status,
  		send_buf, recv_buf);


 /* compute the residual */
 rloc = 0;
 for(i = i_lower; i <= i_upper; i++) {
   for(j = j_lower; j <= j_upper; j++) {
     rloc += ( (P[i+1][j]-2.0*P[i][j]+P[i-1][j])/(dx*dx) + ( P[i][j+1]-2.0*P[i][j]+P[i][j-1])/(dy*dy) - RS[i][j])*
             ( (P[i+1][j]-2.0*P[i][j]+P[i-1][j])/(dx*dx) + ( P[i][j+1]-2.0*P[i][j]+P[i][j-1])/(dy*dy) - RS[i][j]);
   }
 }
 rloc = rloc/(i_upper*j_upper);
 *res = rloc;

}

