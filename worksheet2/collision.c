#include "collision.h"
#include "computeCellValues.h"

void computePostCollisionDistributions(double *currentCell, const double * const tau, const double *const feq)
{

    for (int z = 0; z <19 ; z++)
    {
        currentCell[z] = currentCell[z] - (1.0/ *tau) * ( currentCell[z] - feq[z] );
    }
}

void doCollision(double *collideField, int *flagField,const double * const tau,int xlength)
{
    double density;
    double velocity[3];
    double feq[19];

int dim = xlength +2;

    for (int z = 0; z < dim ; z++)
    {
        for (int y = 0; y < dim ; y++)
        {
            for (int x = 0; x < dim ; x++)
            {
                int localindex = x + dim* y + dim * dim *z;
                /** loops through the matrix taking into account of matrix
                dimension including boundary if xlength+2 **/

                int cell_index = 19*localindex;
                /**** We consider that each cell stores 19 Fi *******/

                if (flagField[localindex] == 0)
                {
                    computeDensity  ( &collideField[cell_index], &density );
                    computeVelocity ( &collideField[cell_index], &density, velocity );
                    computeFeq      ( &density, velocity, feq );
                    computePostCollisionDistributions ( &collideField[cell_index], tau, feq );
                }
            }
        }
    }
}

