#include "boundary.h"
#include "computeCellValues.h"
#include <stdlib.h>

void treatBoundary(double *collideField, int* flagField,
		const double * const velocityWall, int xlength[3], double rhoref, double rhoin ) {

	int dim_x = xlength[0] + 2;
	int dim_y = xlength[1] + 2;
	int dim_z = xlength[2] + 2;

	for (int z = 0; z < dim_z; z++) {
		for (int y = 0; y < dim_y; y++) {
			for (int x = 0; x < dim_x; x++) {
				int localindex = x + dim_x * y + dim_x * dim_y * z;
				int cell_index = Q * localindex;


/*************************  NO SLIP ***************************************************/
				if (flagField[localindex] == 1) {

					for (int q = 0; q < Q; q++)

					{
						int x_x = x + LATTICEVELOCITIES[q][0];
						int y_y = y + LATTICEVELOCITIES[q][1];
						int z_z = z + LATTICEVELOCITIES[q][2];

						if (x_x >= 0 && x_x < dim_x && y_y >= 0 && y_y < dim_y
								&& z_z >= 0 && z_z < dim_z) {
							if (flagField[localindex + LATTICEVELOCITIES[q][0]
									+ dim_x * LATTICEVELOCITIES[q][1]
									+ dim_x * dim_y * LATTICEVELOCITIES[q][2]]
									== 0)

									{
								collideField[cell_index + q] =
										collideField[cell_index
												+ Q
														* (LATTICEVELOCITIES[q][0]
																+ dim_x
																		* LATTICEVELOCITIES[q][1]
																+ dim_x * dim_y
																		* LATTICEVELOCITIES[q][2])
												+ Q - 1 - q];
							}
						}
					}
				}

/******************** MOVING WALL ******************************************************************/
	/*			else if (flagField[localindex] == 2) {

					for (int q = 0; q < Q; q++)

					{
						int x_x = x + LATTICEVELOCITIES[q][0];
						int y_y = y + LATTICEVELOCITIES[q][1];
						int z_z = z + LATTICEVELOCITIES[q][2];

						if (x_x >= 0 && x_x < dim_x && y_y >= 0 && y_y < dim_y
								&& z_z >= 0 && z_z < dim_z) {

							if (flagField[localindex + LATTICEVELOCITIES[q][0]
									+ dim_x * LATTICEVELOCITIES[q][1]
									+ dim_x * dim_y * LATTICEVELOCITIES[q][2]]
									== 0) {

								double n_density;
								computeDensity(
										&collideField[cell_index
												+ Q
														* (LATTICEVELOCITIES[q][0]
																+ dim_x
																		* LATTICEVELOCITIES[q][1]
																+ dim_x * dim_y
																		* LATTICEVELOCITIES[q][2])
												+ q], &n_density);

								collideField[cell_index + q] =
										collideField[cell_index
												+ Q
														* (LATTICEVELOCITIES[q][0]
																+ dim_x
																		* LATTICEVELOCITIES[q][1]
																+ dim_x * dim_y
																		* LATTICEVELOCITIES[q][2])
												+ Q - 1 - q]

												+ ((2.0 * n_density)
														/ C_S_square)
														* LATTICEWEIGHTS[q]
														* (LATTICEVELOCITIES[q][0]
																* velocityWall[0]
																+ LATTICEVELOCITIES[q][1]
																		* velocityWall[1]
																+ LATTICEVELOCITIES[q][2]
																		* velocityWall[2]);
							}
						}
					}
				}
*/
/*********************** Free slip *******************************************/
/*********  This is not a portable function for all planes!!!
works only for YZ plane as free slip
***********/

        else if (flagField[ localindex ] == 3)
        {

            /*** Relevant Lattice directions for YZ plane **/
            int indices[5]          = {3,7,10,13,17};
            for (int D = 0; D < 2; D++)
            {

                /** BOUNDDIRECTION is an array that points normal to the wall **/

                int x_x = x + BOUNDDIRECTION[D][0];
                int y_y = y + BOUNDDIRECTION[D][1];
                int z_z = z + BOUNDDIRECTION[D][2];

                if (x_x >= 0 && x_x < dim_x && y_y >= 0 && y_y < dim_y
                                                && z_z >= 0 && z_z < dim_z)
                {
                    if (flagField[ localindex + BOUNDDIRECTION[D][0]
                                             + dim_x * BOUNDDIRECTION[D][1]
                                             + dim_x * dim_y * BOUNDDIRECTION[D][2]] == 0)
                    {
                        if (D == 0)  /** Positive X direction **/
                        {
                            for (int s = 0; s < 5; ++s)
                            {

                            int q = indices[s];
                            collideField[cell_index + q]
                                 = collideField[cell_index  + Q * ( BOUNDDIRECTION[D][0]
                                                                  + dim_x * BOUNDDIRECTION[D][1]
                                                                  + dim_x * dim_y	* BOUNDDIRECTION[D][2])
                                                                                                    + q-2];
                            }
                        }
                        else if (D ==1) /** Negative X direction **/
                        {
                            for (int s = 0; s < 5; ++s)
                            {
                            int q = Q-1 - indices[s];
                            collideField[cell_index + q]
                                 = collideField[cell_index  + Q * ( BOUNDDIRECTION[D][0]
                                                                  + dim_x * BOUNDDIRECTION[D][1]
                                                                  + dim_x * dim_y	* BOUNDDIRECTION[D][2])
                                                                   + q + 2];

                            }
                        }
                    }
                }
            }
        }
/*********************** Free slip *******************************************/
/*********  This is not a portable function for all planes!!!
works only for XZ plane as free slip
***********/

        else if (flagField[ localindex ] == 7)
        {
            /*** Relevant Lattice directions for XZ plane **/
            int indice_y[5]          = {0,  5,  6,  7, 14};
            int indice_yinv[5]       = {4, 11, 12, 13, 18};

            for (int D = 2; D < 4; D++)
            {

                int x_x = x + BOUNDDIRECTION[D][0];
                int y_y = y + BOUNDDIRECTION[D][1];
                int z_z = z + BOUNDDIRECTION[D][2];

                if (x_x >= 0 && x_x < dim_x && y_y >= 0 && y_y < dim_y
                                                && z_z >= 0 && z_z < dim_z)
                {
                    if (flagField[ localindex + BOUNDDIRECTION[D][0]
                                             + dim_x * BOUNDDIRECTION[D][1]
                                             + dim_x * dim_y * BOUNDDIRECTION[D][2]] == 0)
                    {
                        if (D == 2)   /** Positive Y direction **/
                        {
                            for (int s = 0; s < 5; ++s)
                            {
                            collideField[cell_index + indice_yinv[s] ]
                                 = collideField[cell_index  + Q * ( BOUNDDIRECTION[D][0]
                                                                  + dim_x * BOUNDDIRECTION[D][1]
                                                                  + dim_x * dim_y	* BOUNDDIRECTION[D][2])
                                                                                                    + indice_y[s] ];
                            }
                        }
                        else if (D ==3)   /** Negative Y direction **/
                        {
                            for (int s = 0; s < 5; ++s)
                            {
                            collideField[cell_index + indice_y[s] ]
                                 = collideField[cell_index  + Q * ( BOUNDDIRECTION[D][0]
                                                                  + dim_x * BOUNDDIRECTION[D][1]
                                                                  + dim_x * dim_y	* BOUNDDIRECTION[D][2])
                                                                   + indice_yinv[s] ];

                            }
                        }
                    }
                }
            }
        }

/********************* OUTFLOW ****************************************************/
				else if (flagField[localindex] == 5)
				{
                    double velocity_moved[3];
                    double feq[Q];
                    rhoref =1.0;

					for (int q = 0; q < Q; q++)

					{
						int x_x = x + LATTICEVELOCITIES[q][0];
						int y_y = y + LATTICEVELOCITIES[q][1];
						int z_z = z + LATTICEVELOCITIES[q][2];

						if (x_x >= 0 && x_x < dim_x && y_y >= 0 && y_y < dim_y
								&& z_z >= 0 && z_z < dim_z) {

							if (flagField[localindex + LATTICEVELOCITIES[q][0]
									+ dim_x * LATTICEVELOCITIES[q][1]
									+ dim_x * dim_y * LATTICEVELOCITIES[q][2]]
									== 0) {

								int moved_cellindex = cell_index
									     + Q * (LATTICEVELOCITIES[q][0] + dim_x * LATTICEVELOCITIES[q][1]
												+ dim_x * dim_y	* LATTICEVELOCITIES[q][2]);

			                    computeVelocity ( &collideField[moved_cellindex], &rhoref, velocity_moved );
			                    computeFeq      ( &rhoref, velocity_moved, feq );

								collideField[cell_index + q] = feq[q] + feq[ Q - 1 - q] - collideField[moved_cellindex + (Q - 1)- q];


							}
						}
					}
				}

/********************* INFLOW PRESSURE BC ****************************************************/
				else if (flagField[localindex] == 6)
				{

                    double velocity_moved[3];
                    double feq_moved[Q];

					for (int q = 0; q < Q; q++)

					{
						int x_x = x + LATTICEVELOCITIES[q][0];
						int y_y = y + LATTICEVELOCITIES[q][1];
						int z_z = z + LATTICEVELOCITIES[q][2];

						if (x_x >= 0 && x_x < dim_x && y_y >= 0 && y_y < dim_y
								&& z_z >= 0 && z_z < dim_z) {

							if (flagField[localindex + LATTICEVELOCITIES[q][0]
									+ dim_x * LATTICEVELOCITIES[q][1]
									+ dim_x * dim_y * LATTICEVELOCITIES[q][2]]
									== 0) {

								int moved_cellindex = cell_index
									     + Q * (LATTICEVELOCITIES[q][0] + dim_x * LATTICEVELOCITIES[q][1]
												+ dim_x * dim_y	* LATTICEVELOCITIES[q][2]);

			                    computeVelocity ( &collideField[moved_cellindex], &rhoin, velocity_moved );
			                    computeFeq      ( &rhoin, velocity_moved, feq_moved );

								collideField[cell_index + q] = feq_moved[q] + feq_moved[ Q - 1 - q] - collideField[moved_cellindex + (Q - 1)- q];

							}
						}
					}
				}

/********************* VELOCITY INFLOW *********************************************/


        else if (flagField[localindex] == 4)
        {

            double feq_wall[Q];
            double rho_ref=1.0;

      double CI_U ;

      double U_U ;

      double CI_U_CSCS;

      double CI_U_sq;

      double UU_CS_sq ;

            for (int q = 0; q < Q; q++)

            {
                int x_x = x + LATTICEVELOCITIES[q][0];
                int y_y = y + LATTICEVELOCITIES[q][1];
                int z_z = z + LATTICEVELOCITIES[q][2];

                if (x_x >= 0 && x_x < dim_x && y_y >= 0 && y_y < dim_y
                        && z_z >= 0 && z_z < dim_z) {

                    if (flagField[localindex + LATTICEVELOCITIES[q][0]
                            + dim_x * LATTICEVELOCITIES[q][1]
                            + dim_x * dim_y * LATTICEVELOCITIES[q][2]]
                            == 0) {

                        CI_U =  ( LATTICEVELOCITIES[q][0] * velocityWall[0] +
                                       LATTICEVELOCITIES[q][1] * velocityWall[1] +
                                       LATTICEVELOCITIES[q][2] * velocityWall[2] );

                        U_U =  velocityWall[0] * velocityWall[0] +
                                    velocityWall[1] * velocityWall[1] +
                                    velocityWall[2] * velocityWall[2] ;

                        CI_U_CSCS = CI_U / ( C_S_square );

                        CI_U_sq = (CI_U * CI_U) / ( 2.0 * C_S_square * C_S_square );

                        UU_CS_sq = U_U / (2.0 * C_S_square );

                        feq_wall[q] = (rho_ref) * LATTICEWEIGHTS[q] *( 1.0 + CI_U_CSCS + CI_U_sq - UU_CS_sq );


                        collideField[cell_index + q] = feq_wall[q];

                    }
                }
            }
        }


/******************************************************************************************/

			} /* end x forloop*/
		} /* end y forloop*/
	} /* end z forloop*/
}/* end the function */

