#ifndef _LBDEFINITIONS_H_
#define _LBDEFINITIONS_H_
#define Q 19

  static const int LATTICEVELOCITIES[19][3] = {  {0, -1, -1},
                                                {-1,  0, -1},
                                                 {0,  0, -1},
                                                 {1,  0, -1},
                                                 {0,  1, -1},
                                                {-1, -1,  0},
                                                 {0, -1,  0},
                                                 {1, -1,  0},
                                                {-1,  0,  0},
                                                 {0,  0,  0},
                                                 {1,  0,  0},
                                                {-1,  1,  0},
                                                 {0,  1,  0},
                                                 {1,  1,  0},
                                                 {0, -1,  1},
                                                {-1,  0,  1},
                                                 {0,  0,  1},
                                                 {1,  0,  1},
                                                 {0,  1,  1}  };


  static const double LATTICEWEIGHTS[19] = {    1.0/ 36.0,
                                                1.0/ 36.0,
                                                1.0/ 18.0,
                                                1.0/ 36.0,
                                                1.0/ 36.0,
                                                1.0/ 36.0,
                                                1.0/ 18.0,
                                                1.0/ 36.0,
                                                1.0/ 18.0,
                                                1.0/ 3.0 ,
                                                1.0/ 18.0,
                                                1.0/ 36.0,
                                                1.0/ 18.0,
                                                1.0/ 36.0,
                                                1.0/ 36.0,
                                                1.0/ 36.0,
                                                1.0/ 18.0,
                                                1.0/ 36.0,
                                                1.0/ 36.0   };
static const double C_S = 0.57735026918963 ;

  static const double C_S_square = 1.0/3.0;  /******* Redefined to C_S square we never use C_S ******/

#endif

