#include "helper.h"
#include "visual.h"
#include "init.h"
#include "uvp.h"
#include "boundary_val.h"
#include "sor.h"
#include "parallel.h"
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>


/**
 * The main operation reads the configuration file, initializes the scenario and
 * contains the main loop. So here are the individual steps of the algorithm:
 *
 * - read the program configuration file using read_parameters()
 * - set up the matrices (arrays) needed using the matrix() command
 * - create the initial setup init_uvp(), init_flag(), output_uvp()
 * - perform the main loop
 * - trailer: destroy memory allocated and do some statistics
 *
 * The layout of the grid is described by the first figure below, the enumeration
 * of the whole grid is given by the second figure. All the unknowns correspond
 * to a two dimensional degree of freedom layout, so they are not stored in
 * arrays, but in a matrix.
 *
 * @image html grid.jpg
 *
 * @image html whole-grid.jpg
 *
 * Within the main loop the following big steps are done (for some of the
 * operations a definition is defined already within uvp.h):
 *
 * - calculate_dt() Determine the maximal time step size.
 * - boundaryvalues() Set the boundary values for the next time step.
 * - calculate_fg() Determine the values of F and G (diffusion and convection).
 *   This is the right hand side of the pressure equation and used later on for
 *   the time step transition.
 * - calculate_rs()
 * - Iterate the pressure poisson equation until the residual becomes smaller
 *   than eps or the maximal number of iterations is performed. Within the
 *   iteration loop the operation sor() is used.
 * - calculate_uv() Calculate the velocity at the next time step.
 */


int main(int argn, char** args)
{

	double Re;
    double UI;
    double VI;
    double PI;
    double GX;
    double GY;
    double t_end;
    double xlength;
    double ylength;
    double dt;
    double dx;
    double dy;
    int  imax;
    int  jmax;
    double alpha;
    double omg;
    double tau;
    int  itermax;
    double eps;
    double dt_value;
    double t;
    int it;
    int n;
    double res;
    double res_local;
    int iproc;
    int jproc;
    int myrow, mycol;
    double *send_buf;
    double *recv_buf;

    int i_lower, i_upper;
    int j_lower, j_upper;

    const char *szProblem = "cavity100";
    double **    U = NULL;
    double **    V = NULL;
    double **    P = NULL;

    double **    F  = NULL;
    double **    G  = NULL;
    double **    RS = NULL;

    int myrank, nproc;
    int NeighborID_li, NeighborID_re, NeighborID_bottom, NeighborID_top;
    int recv_NeighborID_li, recv_NeighborID_re, recv_NeighborID_top, recv_NeighborID_bottom;
    MPI_Status status;

    int print_step = 0;

    MPI_Init( &argn, &args );
    MPI_Comm_size( MPI_COMM_WORLD, &nproc );
    MPI_Comm_rank( MPI_COMM_WORLD, &myrank );

    read_parameters( "cavity100.dat", &Re,&UI,&VI,&PI,&GX,&GY,&t_end,&xlength,&ylength,
                    &dt,&dx,&dy,&imax,&jmax,&alpha,&omg,&tau,&itermax,&eps,&dt_value, &iproc, &jproc);

    Program_Message("Read done");

    /** Exit if incorrect processors**/
    if (nproc != iproc * jproc ){
	Programm_Stop("nproc is not equal to iproc*jproc");
    }

	parallel_init( myrank, iproc, jproc, imax, jmax, &myrow, &mycol, &NeighborID_bottom, &NeighborID_top,
			&NeighborID_li, &NeighborID_re, &i_lower, &i_upper, &j_lower, &j_upper);


    /** Memory allocation **/
    P = matrix( i_lower - 1, i_upper + 1, j_lower - 1, j_upper + 1 );
    U = matrix( i_lower - 2, i_upper + 1, j_lower - 1, j_upper + 1 );
    V = matrix( i_lower - 1, i_upper + 1, j_lower - 2, j_upper + 1 );

    F  = matrix( i_lower - 2, i_upper + 1, j_lower - 1, j_upper + 1 );
    G  = matrix( i_lower - 1, i_upper + 1, j_lower - 2, j_upper + 1 );
    RS = matrix( i_lower, i_upper, j_lower, j_upper );

    t = 0;      /**     Initialisation of time                      **/
    n = 0;      /**     Initial time step iteration number = 0      **/

    /********** Initialising the matrices U,V,P in the inner points to initial values UI,VI,PI **********/
    init_uvp(UI,VI,PI,i_lower, i_upper, j_lower, j_upper, U, V, P);

    while( t < t_end)
    {

    	/***** dt computation ******/
    	calculate_dt(Re, tau, &dt, dx, dy, i_lower, i_upper, j_lower, j_upper, U, V);

    	/****** Set boundary values ********/

    	boundaryvalues(i_lower, i_upper, j_lower, j_upper, imax, jmax,
    			NeighborID_li, NeighborID_re, NeighborID_bottom, NeighborID_top, U, V);

    	/******* Calculate FG ********/
    	calculate_fg( Re, GX, GY, alpha, dt, dx, dy,  imax,  jmax,
    				  i_lower, i_upper,  j_lower, j_upper, NeighborID_li,
    				  NeighborID_re, NeighborID_top, NeighborID_bottom, U, V, F,G);
        /***  Calculate RS ***/
    	calculate_rs( dt, dx, dy, i_lower, i_upper, j_lower, j_upper, F, G, RS);

        it=0;
        res = 100;

        /** Do SOR **/
        while( it<itermax && fabs(res) > eps)
        {
        	sor( omg, dx,dy,i_lower,i_upper,j_lower,j_upper, P, RS, &res_local,imax, jmax, NeighborID_bottom,
        			NeighborID_top, NeighborID_li, NeighborID_re, myrank, &status, send_buf, recv_buf);

        /*** compute residual from all processors ***/
    	MPI_Allreduce(&res_local, &res, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    	res_local = sqrt(res);
    	res = res_local;

    	it = it +1;
        }

        /***** calculate UV ****/
        calculate_uv( dt, dx, dy, i_lower, i_upper, j_lower, j_upper, U, V, F, G, P,  NeighborID_li, NeighborID_re, NeighborID_bottom, NeighborID_top);

        /** Communicate U and V to ghost cells **/
        uv_comm(U, V, i_upper, i_lower, j_lower, j_upper,
		NeighborID_li, NeighborID_re, NeighborID_bottom, NeighborID_top,  &status,
		send_buf, recv_buf);

        /** UV comm has an inherint sync **/

    	t = t+ dt;
    	n++;

        /*** printing depends on dt_value in cavity100.dat file **/
    	if ( print_step * dt_value <= t)
    	{

        	if ( myrank == 0){
        	printf(" dt:%f time: %f res : %f SOR time steps : %d \n",dt, t, res, it);
        	}

    		write_vtkFile( szProblem,n, i_lower, i_upper, j_lower, j_upper, dx, dy, U, V, P, myrank);
    		print_step++;
    	}

    }

    /****** Write an output vtk file for visualisation at t = t_end *******/
    write_vtkFile( szProblem,n, i_lower, i_upper, j_lower, j_upper, dx, dy, U, V, P, myrank);
    Program_Message("writing done");

    /****** Free allocated memory for U, V, P, F and G  ******/
    free_matrix(P, i_lower - 1, i_upper + 1, j_lower - 1, j_upper + 1 );
    free_matrix(U, i_lower - 2, i_upper + 1, j_lower - 1, j_upper + 1 );
    free_matrix(V, i_lower - 1, i_upper + 1, j_lower - 2, j_upper + 1 );

    free_matrix(F,  i_lower - 2, i_upper + 1, j_lower - 1, j_upper + 1 );
    free_matrix(G,  i_lower - 1, i_upper + 1, j_lower - 2, j_upper + 1 );
    free_matrix(RS, i_lower, i_upper, j_lower, j_upper );

    Programm_Stop("Program completed and exiting");

	return 0;
}
