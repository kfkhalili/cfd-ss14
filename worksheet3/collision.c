#include "collision.h"
#include "computeCellValues.h"

void computePostCollisionDistributions(double *currentCell, const double * const tau, const double *const feq)
{

    for (int z = 0; z <Q ; z++)
    {
        currentCell[z] = currentCell[z] - (1.0/ *tau) * ( currentCell[z] - feq[z] );
    }
}

void doCollision(double *collideField, int *flagField,const double * const tau, int xlength[3])
{
    double density;
    double velocity[3];
    double feq[Q];

    int dim_x = xlength[0] + 2;
    int dim_y = xlength[1] + 2;
    int dim_z = xlength[2] + 2;

    for (int z = 0; z < dim_z ; z++)
    {
        for (int y = 0; y < dim_y ; y++)
        {
            for (int x = 0; x < dim_x ; x++)
            {
                int localindex = x + dim_x * y + dim_x * dim_y *z;
                int cell_index = Q * localindex;

                if (flagField[localindex] == 0)
                {
                    computeDensity  ( &collideField[cell_index], &density );
                    computeVelocity ( &collideField[cell_index], &density, velocity );
                    computeFeq      ( &density, velocity, feq );
                    computePostCollisionDistributions ( &collideField[cell_index], tau, feq );
                }
            }
        }
    }
}

