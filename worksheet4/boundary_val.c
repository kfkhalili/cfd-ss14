/**
 * The boundary values of the problem are set.
 */
 #include "boundary_val.h"

void boundaryvalues(
    int i_lower,
    int i_upper,
    int j_lower,
    int j_upper,
    int imax,
    int jmax,
    int NeighborID_li,
    int NeighborID_re,
    int NeighborID_bottom,
    int NeighborID_top,
    double **U,
    double **V)
    {
        int i,j;

        for (j= j_lower - 1; j <= (j_upper + 1); j++)
        {
            /** NO SLIP WALL**/

       	 if ( i_lower == 1 )
       	   {

            U[0][j]      =   0;                     /*** LEFT BOUNDARY ***/
            V[0][j]      =  -V[1][j];         		/*** LEFT BOUNDARY ***/
       	   }


    	 if ( i_upper == imax )
    	   {
            /** NO SLIP WALL**/

            U[imax][j]   =   0;                     		/*** RIGHT BOUNDARY ***/
            V[imax + 1][j] =  -V[imax][j];            		/*** RIGHT BOUNDARY ***/
    	   }
        }


        for (i= i_lower - 1; i <= i_upper + 1; i++)
        {

       	 if ( j_lower == 1 )
       	   {
            /** NO SLIP WALL**/

            V[i][ 0 ]      =   0;                     	/*** BOTTOM BOUNDARY ***/
            U[i][ 0 ]      =  -U[i][ 1 ];               /*** BOTTOM BOUNDARY ***/
       	   }

    	 if ( j_upper == jmax )
    	   {
    		 /** MOVING WALL**/

            V[i][jmax]   =   0;                     	/*** TOP BOUNDARY ***/
            U[i][jmax + 1] =   2.0 - U[i][jmax];       /*** TOP BOUNDARY ***/

    	   }
        }
}
