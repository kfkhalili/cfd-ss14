#include "initLB.h"

int readParameters(int *xlength, double *tau, double *velocityWall, int *timesteps, int *timestepsPerPlotting, int argc, char *argv[]){

  if (argc == 2)
  {

        read_int(argv[1],"xlength",xlength);
        read_double(argv[1],"tau",tau);
        read_double(argv[1],"XvelocityWall", &velocityWall[0]);
        read_double(argv[1],"YvelocityWall", &velocityWall[1]);
        read_double(argv[1],"ZvelocityWall", &velocityWall[2]);

        read_int(argv[1],"timesteps",timesteps);
        read_int(argv[1],"timestepsPerPlotting", timestepsPerPlotting);


  return 0;
  }

  else
  {
      return 1;

  }

}


void initialiseFields(double *collideField, double *streamField, int *flagField, int xlength){


int dim = xlength +2 ;
  for (int z = 0; z < dim ; z++)
  {
      for (int y = 0; y < dim ; y++)
      {
          for (int x = 0; x < dim ; x++)
          {
                int loopindex = x + y*dim + dim*dim*z;
             
                if (x == 0 || x == xlength +1 || y == 0 ||y== xlength +1 || z == 0 || z== xlength + 1  )
                {
                    flagField[loopindex] = 1;  /***** NO SLIP  ***/
                }

                else
                {
                    flagField[loopindex] = 0;  /***** FLUID *******/
                }
          }
      }
  }

  for (int z = 0; z < dim ; z++)
  {
      for (int y = 0; y < dim ; y++)
      {
          for (int x = 0; x < dim ; x++)
          {
                int loopindex = x + y*dim + dim*dim*z;

                 if (y == xlength +1)
                {
                    flagField[loopindex] = 2;  /**** MOVING WALL ****/
                }               

          }
      }
  }





  for (int z = 0; z < dim ; z++)
  {
      for (int y = 0; y < dim ; y++)
      {
          for (int x = 0; x < dim; x++)
          {
                int loopindex = x + y*dim + dim*dim*z;

                int cell_index = 19* loopindex;

                for (int q = 0; q < 19; q++ )
                {
                    collideField[cell_index + q] = LATTICEWEIGHTS[q];
                    streamField [cell_index + q] = LATTICEWEIGHTS[q];
                }
          }
      }
  }
}
