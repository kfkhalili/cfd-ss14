/**
 * The boundary values of the problem are set.
 */
 #include "boundary_val.h"

void boundaryvalues(
    int imax,
    int jmax,
    double **U,
    double **V)
    {
        int i,j;

        for (j=1; j <= (jmax+1); j++)
        {
            /** NO SLIP WALL**/

            U[0][j]      =   0;                     /*** LEFT BOUNDARY ***/
            V[0][j]      =  -V[1][j];               /*** LEFT BOUNDARY ***/

            /** NO SLIP WALL**/

            U[imax][j]   =   0;                     /*** RIGHT BOUNDARY ***/
            V[imax+1][j] =  -V[imax][j];            /*** RIGHT BOUNDARY ***/
       }

        for (i=0; i<=imax+1; i++)
        {
            /** NO SLIP WALL**/

            V[i][0]      =   0;                     /*** BOTTOM BOUNDARY ***/
            U[i][0]      =  -U[i][1];               /*** BOTTOM BOUNDARY ***/

            /** MOVING WALL**/

            V[i][jmax]   =   0;                     /*** TOP BOUNDARY ***/
            U[i][jmax+1] =   2.0 - U[i][jmax];       /*** TOP BOUNDARY ***/
        }
    }
