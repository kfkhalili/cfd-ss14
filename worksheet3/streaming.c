#include "streaming.h"
#include "LBDefinitions.h"

void doStreaming(double *collideField, double *streamField,int *flagField,int *xlength )
{

    int dim_x = xlength[0] + 2;
    int dim_y = xlength[1] + 2;
    int dim_z = xlength[2] + 2;

    int xpos = Q;
    int ypos = Q * dim_x;
    int zpos = Q * dim_x * dim_y;

    for (int z = 0; z < dim_z ; z++)
    {
        for (int y = 0; y < dim_y ; y++)
        {
            for (int x = 0; x < dim_x ; x++)
            {
                int localindex = x + dim_x *y + dim_x * dim_y *z;
                /** loops through the matrix taking into account of matrix
                dimension including boundary if xlength+2 **/

                int cell_index = Q * localindex;
                /**** We consider that each cell stores 19 Fi *******/

                /*** Only for fluid cells do streaming  **/
                if (flagField[ localindex ] == 0)
                {

                    for (int q=0; q < Q ; q++){
                    streamField[ cell_index + q ] = collideField[ cell_index - xpos*LATTICEVELOCITIES[q][0]
                    - ypos* LATTICEVELOCITIES[q][1] - zpos* LATTICEVELOCITIES[q][2] + q ];
                    }

                }
            }
        }
    }
}

