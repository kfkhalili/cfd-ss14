#include "boundary.h"
#include "computeCellValues.h"


void treatBoundary(double *collideField, int* flagField, const double * const wallVelocity, int xlength){

    int dim = xlength +2;
/*
     int xpos = 19;
     int ypos = 19*dim;
     int zpos = 19*dim*dim;
*/
    for (int z = 0; z < dim ; z++)
    {
        for (int y = 0; y < dim ; y++)
        {
            for (int x = 0; x < dim ; x++)
            {
                int localindex = x + dim*y + dim*dim*z;
                int cell_index = 19*localindex;


                if (flagField[localindex]==1)
                {

                    for(int q =0; q < 19 ; q++)

                    {
                        int x_x = x + LATTICEVELOCITIES[q][0];
                        int y_y = y + LATTICEVELOCITIES[q][1];
                        int z_z = z + LATTICEVELOCITIES[q][2];


                        if ( x_x >= 0 && x_x < dim && y_y >= 0 && y_y <dim && z_z >= 0 && z_z < dim)
                         {
                                if (flagField[localindex + LATTICEVELOCITIES[q][0] + dim*LATTICEVELOCITIES[q][1] + dim*dim*LATTICEVELOCITIES[q][2]] == 0)

                                {
                                    collideField[cell_index + q] = collideField[cell_index+ 19*
                                    ( LATTICEVELOCITIES[q][0] + dim*LATTICEVELOCITIES[q][1] + dim*dim*LATTICEVELOCITIES[q][2]) + 18-q]; 
                                }
                          }
                     }
                }

                if (flagField[localindex]==2)
                {

                    for(int q =0; q < 19 ; q++)

                    {
                        int x_x = x + LATTICEVELOCITIES[q][0];
                        int y_y = y + LATTICEVELOCITIES[q][1];
                        int z_z = z + LATTICEVELOCITIES[q][2];


                        if ( x_x >= 0 && x_x < dim && y_y >= 0 && y_y <dim && z_z >= 0 && z_z < dim)
                        {


                                if ( flagField[localindex + LATTICEVELOCITIES[q][0] + dim*LATTICEVELOCITIES[q][1] + dim*dim*LATTICEVELOCITIES[q][2]] == 0)
                                {

                                double n_density;
                                computeDensity  ( &collideField[cell_index + 19*
                                ( LATTICEVELOCITIES[q][0] + dim*LATTICEVELOCITIES[q][1] + dim*dim*LATTICEVELOCITIES[q][2]) +q ], &n_density );

                                collideField[cell_index + q] = collideField[cell_index+ 19*
                                ( LATTICEVELOCITIES[q][0] + dim*LATTICEVELOCITIES[q][1] + dim*dim*LATTICEVELOCITIES[q][2]) + 18-q]

                                + ((2.0*n_density)/C_S_square)* LATTICEWEIGHTS[q]* ( LATTICEVELOCITIES[q][0] * wallVelocity[0]
                                + LATTICEVELOCITIES[q][1]* wallVelocity[1] + LATTICEVELOCITIES[q][2]* wallVelocity[2] ) ; 
                                }
                        }
                    }
                  }

            } 
        }
    }
}


