#ifndef _BOUNDARY_H_
#define _BOUNDARY_H_

#include "LBDefinitions.h"
#include <stdlib.h>
#include <stdio.h>

/** handles the boundaries in our simulation setup */
void treatBoundary(double *collideField, int* flagField, const double * const velocityWall,
                   int xlength[3], double rhoref, double rhoin);

#endif

