#ifndef _PAR_
#define _PAR_
#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include "helper.h"

void Program_Message(char *txt);
/* produces a stderr text output  */



void Programm_Sync(char *txt);
/* produces a stderr textoutput and synchronize all processes */



void Programm_Stop(char *txt);
/* all processes will produce a text output, be synchronized and finished */

void parallel_init(int myrank, int iproc, int jproc, int imax, int jmax, int *myrow, int *mycol, int *NeighborID_bottom, int *NeighborID_top,
		int *NeighborID_li, int *NeighborID_re, int *i_lower, int *i_upper, int *j_lower, int *j_upper);
/* Will do a domain decomposition and assign neighbors including MPI_PROC_NULL where der are no neighbors */


void uv_comm(double **U, double **V, int i_upper, int i_lower, int j_lower, int j_upper,
		int NeighborID_li, int NeighborID_re, int NeighborID_bottom, int NeighborID_top,  MPI_Status *status,
		double *send_buf, double *recv_buf);
/*  communicate UV at ghost cells*/

void pressure_comm(double **P, int i_upper, int i_lower, int j_lower, int j_upper,
		int NeighborID_li, int NeighborID_re, int NeighborID_bottom, int NeighborID_top,  MPI_Status *status,
		double *send_buf, double *recv_buf);
/* communicate P at ghost cells*/

#endif
