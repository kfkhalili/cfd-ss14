#ifndef __RANDWERTE_H__
#define __RANDWERTE_H__

#include <mpi.h>

/**
 * The boundary values of the problem are set.
 */
void boundaryvalues(
    int i_lower,
    int i_upper,
    int j_lower,
    int j_upper,
    int imax,
    int jmax,
    int NeighborID_li,
    int NeighborID_re,
    int NeighborID_bottom,
    int NeighborID_top,
    double **U,
    double **V);

#endif
