#include<tgmath.h>
#include<stdlib.h>


void calculate_dt(
    double Re,
    double tau,
    double *dt,
    double dx,
    double dy,
    int imax,
    int jmax,
    double **U,
    double **V)
    {

        if (tau > 0)
        {
            double dtRE, dtUmax, dtVmax,Umax, Vmax;
            int i,j;
            Umax=0.0;
            Vmax=0.0;


            for (i=0;i<=imax+1;i++)
            {
                for(j=0;j<=jmax+1;j++)
                {
                    Umax=fmax(Umax,U[i][j]);
                    Vmax=fmax(Vmax,V[i][j]);
                }
            }

            dtRE   =   Re  / ( 2.0 *( 1.0/pow(dx,2) + 1.0/pow(dy,2)));
            dtUmax   =   dx  / Umax;
            dtVmax   =   dy  / Vmax;

            *dt = tau* fmin(fmin(dtUmax,dtVmax),dtRE);
        }
    }

void calculate_fg(double Re,double GX,double GY,double alpha,double dt,double
dx,double dy,int imax,int jmax,double **U,double **V,double **F,double **G)
{
    double du2_dx, duv_dy,lapu_xy, dv2_dy, duv_dx, lapv_xy;
    int i,j;

    /**Boundary conditions for F and G (boundary grid points)*/

    for (j=1; j<=jmax; j++)
    {
        F[0][j] = U[0][j];
        F[imax][j] = U[imax][j];
    }
    for (i=1; i<=imax; i++)
    {
        G[i][0] = V[i][0];
        G[i][jmax] = V[i][jmax];
    }

    /*** Computation at the inner grid points for F and G ***/

    for (i=1; i<=imax-1; i++)
    {
        for (j=1; j<=jmax; j++)
        {
            du2_dx = ((U[i][j] + U[i+1][j]) * (U[i][j] + U[i+1][j]) - (U[i-1][j] + U[i][j]) * (U[i-1][j] + U[i][j]) +
                    alpha * (fabs(U[i][j] + U[i+1][j]) * (U[i][j] - U[i+1][j])) - alpha * (fabs(U[i-1][j] + U[i][j]) *
                    (U[i-1][j] - U[i][j]))) / (dx*4.0) ;

            duv_dy = (((V[i][j] + V[i+1][j]) * (U[i][j] + U[i][j+1]) - (V[i][j-1] + V[i+1][j-1]) * (U[i][j-1] + U[i][j])) +
                    alpha * (fabs(V[i][j] + V[i+1][j]) * (U[i][j] - U[i][j+1])) - alpha*(fabs(V[i][j-1] + V[i+1][j-1]) *
                    (U[i][j-1] - U[i][j]))) / (dy*4.0);

            lapu_xy = (U[i+1][j]-2.0*U[i][j]+U[i-1][j]) / (dx * dx)+
                   (U[i][j+1]-2.0*U[i][j]+U[i][j-1]) / (dy * dy);

            F[i][j] = U[i][j] + dt * ((lapu_xy/Re)- du2_dx - duv_dy + GX);
        }
    }

    for (i=1; i<=imax; i++)
    {
        for (j=1; j<=jmax-1; j++)
        {
            dv2_dy  = ((V[i][j] + V[i][j+1]) * (V[i][j] + V[i][j+1]) - (V[i][j-1] + V[i][j]) * (V[i][j-1] + V[i][j]) +
                    alpha * (fabs(V[i][j] + V[i][j+1]) * (V[i][j] - V[i][j+1])) - alpha * (fabs(V[i][j-1] + V[i][j]) *
                    (V[i][j-1] - V[i][j]))) / (dy*4.0) ;

            duv_dx  = (((U[i][j] + U[i][j+1]) * (V[i][j] + V[i+1][j]) - (U[i-1][j] + U[i-1][j+1]) * (V[i-1][j] + V[i][j])) +
                    alpha * (fabs(U[i][j] + U[i][j+1]) * (V[i][j] - V[i+1][j])) - alpha*(fabs(U[i-1][j] + U[i-1][j+1]) *
                    (V[i-1][j] - V[i][j]))) / (dx*4.0);

            lapv_xy = (V[i+1][j]-2.0*V[i][j]+V[i-1][j]) / (dx * dx) + (V[i][j+1]-2.0*V[i][j]+V[i][j-1]) / (dy * dy);

            G[i][j] = V[i][j] + dt * ((lapv_xy/Re) - dv2_dy - duv_dx + GY);
        }
    }
}

void calculate_rs(
    double dt,
    double dx,
    double dy,
    int imax,
    int jmax,
    double **F,
    double **G,
    double **RS)
    {
        int i,j;
        for (i=1;i<=imax;i++)
        {
            for (j=1;j<=jmax;j++)
            {
                RS[i][j] = ( (F[i][j] - F[i-1][j])/dx + ( G[i][j] - G[i][j-1])/dy  )/dt;
            }
        }
    }

void calculate_uv(
    double dt,
    double dx,
    double dy,
    int imax,
    int jmax,
    double **U,
    double **V,
    double **F,
    double **G,
    double **P)
    {
        int i,j;
        for (i=1;i<=imax-1;i++)
        {
            for (j=1;j<=jmax;j++)
            {
                U[i][j] = F[i][j] - (dt/dx)* ( P[i+1][j] - P[i][j] );
            }
        }

        for (i=1;i<=imax;i++)
        {
            for (j=1;j<=jmax-1;j++)
            {
                V[i][j] = G[i][j] - (dt/dy)* ( P[i][j+1] - P[i][j] );
            }
        }
    }



