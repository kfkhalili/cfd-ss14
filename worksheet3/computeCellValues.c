#include "computeCellValues.h"

/****** C_S square is C_S_square in LBDefinitions *******/

void computeDensity(const double *const currentCell, double *density){

 double sum = 0.0;

  for (int i = 0; i < Q; i++)
  {
      sum = sum + currentCell[i];
  }
*density = sum;
}

void computeVelocity(const double * const currentCell, const double * const density, double *velocity){

double sum[3] = {0.0, 0.0, 0.0};

    for (int i = 0; i < Q; i++ )
    {
        sum[0] = sum[0] + currentCell[i] * (double) LATTICEVELOCITIES[i][0];
        sum[1] = sum[1] + currentCell[i] * (double) LATTICEVELOCITIES[i][1];
        sum[2] = sum[2] + currentCell[i] * (double) LATTICEVELOCITIES[i][2];

    }
velocity[0] = sum[0] / *density;
velocity[1] = sum[1] / *density;
velocity[2] = sum[2] / *density;

}

void computeFeq(const double * const density, const double * const velocity, double *feq){


  for (int i = 0; i < Q; i++)
  {
      double CI_U =  ( LATTICEVELOCITIES[i][0] * velocity[0] +
                       LATTICEVELOCITIES[i][1] * velocity[1] +
                       LATTICEVELOCITIES[i][2] * velocity[2] );

      double U_U =  velocity[0] * velocity[0] +
                   velocity[1] * velocity[1] +
                   velocity[2] * velocity[2] ;

      double CI_U_CSCS = CI_U / ( C_S_square );

      double CI_U_sq = (CI_U * CI_U) / ( 2.0 * C_S_square * C_S_square );

      double UU_CS_sq = U_U / (2.0 * C_S_square );

      feq[i] = (*density) * LATTICEWEIGHTS[i] *( 1.0 + CI_U_CSCS + CI_U_sq - UU_CS_sq );
  }

}

