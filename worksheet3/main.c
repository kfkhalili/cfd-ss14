#ifndef _MAIN_C_
#define _MAIN_C_

#include "collision.h"
#include "streaming.h"
#include "initLB.h"
#include "visualLB.h"
#include "boundary.h"

int main(int argc, char *argv[])
{
    double * collideField = NULL;
    double * streamField = NULL;
    int * flagField = NULL;
    int xlength[3];
    double tau;
    double rhoref;
    double rhoin;
    double velocityWall[3] = {0,0,0};
    int timesteps;
    int timestepsPerPlotting;
    int inflow;

/***  To check if the user has given the input file name ***/
    if ((readParameters(xlength , &tau, velocityWall, &timesteps, &timestepsPerPlotting, argc, argv, &rhoref, &rhoin, &inflow)== 1 ))
    {
        printf("INVALID ARGUMENTS!!\nUSAGE: ./lbsim inputfilename caseNumber\n");
        return 1;
    }
    int dim_x = xlength[0] + 2;
    int dim_y = xlength[1] + 2;
    int dim_z = xlength[2] + 2;

/******** Memory Allocation *******/
    collideField = (double *)malloc((size_t)(19* dim_x * dim_y * dim_z * sizeof(double)));
    streamField = (double *)malloc ((size_t)(19* dim_x * dim_y * dim_z * sizeof(double)));

    flagField = (int *)malloc((size_t)( dim_x * dim_y * dim_z * sizeof(int)) );

/********** Initialisation of collide field and stream field and setting flags for boundary**********/
    initialiseFields(collideField, streamField, flagField, xlength, argv, inflow);


for (int t=0; t< timesteps; t++)
{
    double *swap = NULL;

    /**streaming step */
    doStreaming (collideField, streamField, flagField, xlength);

    swap = collideField;
    collideField = streamField;

    streamField = swap;

    /** collision step **/
    doCollision(collideField, flagField, &tau, xlength );

    /** Boundary setup **/
    treatBoundary(collideField, flagField, velocityWall, xlength, rhoref, rhoin);

    if (t% timestepsPerPlotting == 0)
    {
        /***** output a paraview file at specified time intervals  ******/
        /****** Note that here the full domain including boundaries have been output,
        the the cell center values have been projected to points instead of assigning cell
        values I had another code writing it as cell values but decided to send this one as
        it is easier for you, in that you would have to use paraview filter ->convert cell data
        to point data *****/

    	writeVtkOutput(collideField, flagField, argv[1], t, xlength);

    }

}
writeVtkOutput(collideField, flagField, argv[1], timesteps, xlength);

/**  freeing allocated memory **/
free(collideField);
free(streamField);
free(flagField);

  return 0;
}

#endif

