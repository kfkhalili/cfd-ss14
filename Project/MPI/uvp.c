#include<tgmath.h>
#include<stdlib.h>
#include<stdio.h>
#include <mpi.h>

void calculate_dt(
    double Re,
    double tau,
    double *dt,
    double dx,
    double dy,
    int i_lower,
    int i_upper,
    int j_lower,
    int j_upper,
    double **U,
    double **V)
    {

        if (tau > 0)
        {
            double dtRE, dtUmax, dtVmax,Umax_local, Vmax_local, Umax, Vmax;
            int i,j;
            Umax_local = 0.0;
            Vmax_local = 0.0;


            for (i= i_lower; i <= i_upper + 1; i++)
            {
                for(j= j_lower; j <= j_upper + 1; j++)
                {
                    Umax_local = fmax( Umax_local, U[i][j] );
                    Vmax_local = fmax( Vmax_local, V[i][j] );
                }
            }

            /*** Getting Umax and Vmax from all processor and then broadcasting ***/

            MPI_Allreduce(&Umax_local, &Umax, 1,  MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
            MPI_Allreduce(&Vmax_local, &Vmax, 1,  MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

            dtRE   =   Re  / ( 2.0 *( 1.0/pow( dx, 2 ) + 1.0/pow( dy, 2) ));
            dtUmax   =   dx  / Umax;
            dtVmax   =   dy  / Vmax;

            *dt = tau* fmin(fmin(dtUmax,dtVmax),dtRE);
        }
    }

void calculate_fg(double Re,double GX,double GY,double alpha,double dt,
				double dx,double dy, int imax, int jmax,
				int i_lower,int i_upper, int j_lower, int j_upper,
				int NeighborID_li, int NeighborID_re, int NeighborID_top, int NeighborID_bottom,
				double **U,double **V,double **F,double **G)
{
    double du2_dx, duv_dy,lapu_xy, dv2_dy, duv_dx, lapv_xy;
    int i,j;

    /**Boundary conditions for F and G (boundary grid points)*/

    if ( NeighborID_li == MPI_PROC_NULL)
    {
		for (j= j_lower - 1; j <= j_upper + 1; j++)
		{
			F[0][j] = U[0][j];
		}
    }
    if ( NeighborID_re == MPI_PROC_NULL )
    {
		for (j = j_lower - 1 ; j <= j_upper + 1; j++)
		{
			F[imax][j] = U[imax][j];
		}
    }

    if ( NeighborID_bottom == MPI_PROC_NULL )
    for (i = i_lower - 1; i <= i_upper + 1; i++)
    {
        G[i][0] = V[i][0];
    }
    if ( NeighborID_top == MPI_PROC_NULL )
    for ( i = i_lower - 1; i <= i_upper + 1; i++)
    {
        G[i][jmax] = V[i][jmax];
    }


    /*** Computation at the inner grid points for F and G ***/

	int il = i_lower == 1? 		i_lower 	: i_lower - 1;
	int ir = i_upper == imax? 	i_upper - 1 : i_upper;


    for (i= il; i<= ir; i++)
    {
        for (j= j_lower; j<= j_upper; j++)
        {
            du2_dx = ((U[i][j] + U[i+1][j]) * (U[i][j] + U[i+1][j]) - (U[i-1][j] + U[i][j]) * (U[i-1][j] + U[i][j]) +
                    alpha * (fabs(U[i][j] + U[i+1][j]) * (U[i][j] - U[i+1][j])) - alpha * (fabs(U[i-1][j] + U[i][j]) *
                    (U[i-1][j] - U[i][j]))) / (dx*4.0) ;

            duv_dy = (((V[i][j] + V[i+1][j]) * (U[i][j] + U[i][j+1]) - (V[i][j-1] + V[i+1][j-1]) * (U[i][j-1] + U[i][j])) +
                    alpha * (fabs(V[i][j] + V[i+1][j]) * (U[i][j] - U[i][j+1])) - alpha*(fabs(V[i][j-1] + V[i+1][j-1]) *
                    (U[i][j-1] - U[i][j]))) / (dy*4.0);

            lapu_xy = (U[i+1][j]-2.0*U[i][j]+U[i-1][j]) / (dx * dx)+
                   (U[i][j+1]-2.0*U[i][j]+U[i][j-1]) / (dy * dy);

            F[i][j] = U[i][j] + dt * ((lapu_xy/Re)- du2_dx - duv_dy + GX);
        }
    }

	int jb = j_lower ==1? 		j_lower   : j_lower-1;
	int jt = j_upper ==jmax? 	j_upper-1 : j_upper;


    for (i= i_lower; i <= i_upper; i++)
    {
        for (j= jb; j <= jt; j++)
        {
            dv2_dy  = ((V[i][j] + V[i][j+1]) * (V[i][j] + V[i][j+1]) - (V[i][j-1] + V[i][j]) * (V[i][j-1] + V[i][j]) +
                    alpha * (fabs(V[i][j] + V[i][j+1]) * (V[i][j] - V[i][j+1])) - alpha * (fabs(V[i][j-1] + V[i][j]) *
                    (V[i][j-1] - V[i][j]))) / (dy*4.0) ;

            duv_dx  = (((U[i][j] + U[i][j+1]) * (V[i][j] + V[i+1][j]) - (U[i-1][j] + U[i-1][j+1]) * (V[i-1][j] + V[i][j])) +
                    alpha * (fabs(U[i][j] + U[i][j+1]) * (V[i][j] - V[i+1][j])) - alpha*(fabs(U[i-1][j] + U[i-1][j+1]) *
                    (V[i-1][j] - V[i][j]))) / (dx*4.0);

            lapv_xy = (V[i+1][j]-2.0*V[i][j]+V[i-1][j]) / (dx * dx) + (V[i][j+1]-2.0*V[i][j]+V[i][j-1]) / (dy * dy);

            G[i][j] = V[i][j] + dt * ((lapv_xy/Re) - dv2_dy - duv_dx + GY);
        }
    }
}

void calculate_rs(
    double dt,
    double dx,
    double dy,
    int i_lower,
    int i_upper,
    int j_lower,
    int j_upper,
    double **F,
    double **G,
    double **RS)
    {
        int i,j;
        for (i = i_lower; i <= i_upper; i++)
        {
            for (j= j_lower; j <= j_upper; j++)
            {
                RS[i][j] = ( (F[i][j] - F[i-1][j])/dx + ( G[i][j] - G[i][j-1])/dy  )/dt;
            }
        }
    }

void calculate_uv(
    double dt,
    double dx,
    double dy,
    int i_lower,
    int i_upper,
    int j_lower,
    int j_upper,
    double **U,
    double **V,
    double **F,
    double **G,
    double **P,
    int NeighborID_li,
    int NeighborID_re,
    int NeighborID_bottom,
    int NeighborID_top
    )
    {
        int i,j;

        int il = NeighborID_li == MPI_PROC_NULL ? i_lower    : i_lower -1 ;
        int ir = NeighborID_re == MPI_PROC_NULL ? i_upper -1 : i_upper ;

        for (i= il; i <= ir; i++)
        {
            for (j= j_lower; j <= j_upper; j++)
            {
                U[i][j] = F[i][j] - (dt/dx)* ( P[i+1][j] - P[i][j] );
            }
        }

        int jb = NeighborID_bottom == MPI_PROC_NULL ? j_lower : j_lower - 1;
        int jt = NeighborID_top    == MPI_PROC_NULL ? j_upper -1 : j_upper;

        for (i= i_lower; i <= i_upper; i++)
        {
            for (j= jb ; j <= jt; j++)
            {
                V[i][j] = G[i][j] - (dt/dy)* ( P[i][j+1] - P[i][j] );
            }
        }
    }



