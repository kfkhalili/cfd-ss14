#include "helper.h"
#include "visualLB.h"
#include "LBDefinitions.h"
#include "computeCellValues.h"
#include <stdio.h>

void write_vtkPointCoordinates( FILE *fp, int xlength)
{
  double originX = 0.0;
  double originY = 0.0;
  double originZ = 0.0;

  for(int k = 0; k < xlength+2; k++){
    for(int j = 0; j < xlength+2; j++) {
      for(int i = 0; i < xlength+2; i++) {
        fprintf(fp, "%f %f %f \n", originX+i, originY+j, originZ+k);
      }
    }
  }


}

void write_vtkHeader( FILE *fp, int xlength )
{

  fprintf(fp,"# vtk DataFile Version 2.0\n");
  fprintf(fp,"generated by CFD-lab course output (written by Tobias Neckel) \n");
  fprintf(fp,"ASCII\n");
  fprintf(fp,"\n");
  fprintf(fp,"DATASET STRUCTURED_GRID\n");
  fprintf(fp,"DIMENSIONS  %i %i %i \n", xlength+2, xlength+2, xlength+2);
  fprintf(fp,"POINTS %i float\n", (xlength+2)*(xlength+2)*(xlength+2) );
  fprintf(fp,"\n");
}

void writeVtkOutput(const double * const collideField, const int * const flagField, const char * filename, unsigned int t, int xlength) {
 
  int i,j,k;
  char szFileName[80];
  double density=0.0;
  double velocity[3];
  velocity[0]=0.0; velocity[1]=0.0; velocity[2]=0.0;
  FILE *fp=NULL;
  sprintf( szFileName, "%s.%i.vtk","cube", t );
  fp = fopen( szFileName, "w");
  if( fp == NULL )
  {
    char szBuff[80];
    sprintf( szBuff, "Failed to open %s", szFileName );
    ERROR( szBuff );
    return;
  }


  write_vtkHeader( fp, xlength );

  write_vtkPointCoordinates( fp, xlength);

  fprintf(fp,"POINT_DATA %i \n", (xlength+2)*(xlength+2)*(xlength+2) );
  fprintf(fp,"\n");
  fprintf(fp, "VECTORS velocity float\n");
  for(k = 0; k < xlength+2; k++) {
    for(j = 0; j < xlength+2; j++) {
      for(i = 0; i < xlength+2; i++) {
	density=0.0;
    velocity[0]=0; velocity[1] =0; velocity[2]= 0;
    computeDensity(&collideField[19*((xlength+2)*(xlength+2)*k+(xlength+2)*j+i)], &density);
    computeVelocity(&collideField[19*((xlength+2)*(xlength+2)*k+(xlength+2)*j+i)], &density,velocity);
	fprintf(fp, "%f %f %f\n", velocity[0], velocity[1], velocity[2]);
      }
    }
  }

  fprintf(fp,"\n");
  fprintf(fp, "SCALARS density float 1\n");
  fprintf(fp, "LOOKUP_TABLE default \n");
  for(k = 0; k < xlength+2; k++) {
    for(j = 0; j < xlength+2; j++) {
      for(i = 0; i < xlength+2; i++) {
	density=0.0;
    computeDensity(&collideField[19*((xlength+2)*(xlength+2)*k+(xlength+2)*j+i)], &density);
        fprintf(fp, "%f\n", density );
      }
    }
  }

  if( fclose(fp) )
  {
    char szBuff[80];
    sprintf( szBuff, "Failed to close %s", szFileName );
    ERROR( szBuff );
  }

}

