#include "initLB.h"
#include <stdlib.h>

int readParameters(int *xlength, double *tau, double *velocityWall,
		int *timesteps, int *timestepsPerPlotting, int argc, char *argv[],
		double *rhoref, double *rhoin, int *inflow) {

	if (argc == 3) {

	    printf("The chosen scenario is: %i of 3\n", atoi(argv[2]));

		if (atoi(argv[2]) == 1) {
			read_int(argv[1], "xlength1", &xlength[0]);
			read_int(argv[1], "ylength1", &xlength[1]);
			read_int(argv[1], "zlength1", &xlength[2]);
			read_int(argv[1], "timesteps1", timesteps);
			read_int(argv[1], "inflow1", inflow);

			read_double(argv[1], "velocityWallx1", &velocityWall[0]);
			read_double(argv[1], "velocityWally1", &velocityWall[1]);
			read_double(argv[1], "velocityWallz1", &velocityWall[2]);
			read_double(argv[1], "tau1", tau);
			read_int(argv[1], "timestepsPerPlotting", timestepsPerPlotting);
			return 0;
		} else if (atoi(argv[2]) == 2) {
			read_int(argv[1], "xlength2", &xlength[0]);
			read_int(argv[1], "ylength2", &xlength[1]);
			read_int(argv[1], "zlength2", &xlength[2]);
			read_int(argv[1], "timesteps2", timesteps);
			read_int(argv[1], "inflow2", inflow);

			read_double(argv[1], "rhoref", rhoref);
			read_double(argv[1], "rhoin", rhoin);
			read_double(argv[1], "tau2", tau);
			read_int(argv[1], "timestepsPerPlotting", timestepsPerPlotting);
			return 0;
		} else if (atoi(argv[2]) == 3) {
			read_int(argv[1], "xlength3", &xlength[0]);
			read_int(argv[1], "ylength3", &xlength[1]);
			read_int(argv[1], "zlength3", &xlength[2]);
			read_int(argv[1], "timesteps3", timesteps);
			read_int(argv[1], "inflow3", inflow);

			read_double(argv[1], "velocityWallx3", &velocityWall[0]);
			read_double(argv[1], "velocityWally3", &velocityWall[1]);
			read_double(argv[1], "velocityWallz3", &velocityWall[2]);
			read_double(argv[1], "tau3", tau);
			read_int(argv[1], "timestepsPerPlotting", timestepsPerPlotting);
			return 0;
		} else {
			printf("Please input a valid scenario type:\n"
					"1. TiltedPlate\n"
					"2. PlaneShear\n"
					"3. FlowOverStep\n"
					"example: ./lbsim cube.dat 1\n");
			return 1;
		}
		return 0;
	} else {
		printf(
				"Please input one of the three scenario type after ./lbsim cube.dat:\n"
						"1. TiltedPlate\n"
						"2. PlaneShear\n"
						"3. FlowOverStep\n"
						"example: ./lbsim cube.dat 1\n");
		return 1;
	}
}

void initialiseFields(double *collideField, double *streamField, int *flagField,
		int xlength[3], char *argv[], int inflow) {

	int dim_x = xlength[0] + 2;
	int dim_y = xlength[1] + 2;
	int dim_z = xlength[2] + 2;
	int x, y, z, loopindex;


    /***** FLUID  0 ***/

	for (z = 0; z < dim_z; z++) {
		for (y = 0; y < dim_y; y++) {
			for (x = 0; x < dim_x; x++) {
				loopindex = x + dim_x * y + dim_x * dim_y * z;
				flagField[loopindex] = 0;
			}
		}
	}


	/***** INFLOW  ** 4 for velocity, 6 for pressure BC***/

	z = 0;

	for (y = 0; y < dim_y; y++) {
		for (x = 0; x < dim_x; x++) {
			loopindex = x + y * dim_x + dim_x * dim_y * z;
			flagField[loopindex] = inflow;
		}
	}

	/***** OUTFLOW  *** 5 ****/

	z = dim_z - 1;

	for (y = 0; y < dim_y; y++) {
		for (x = 0; x < dim_x; x++) {
			loopindex = x + y * dim_x + dim_x * dim_y * z;
			flagField[loopindex] = 5;
		}
	}



	/***** NO SLIP  ** 1 ****/

	y = 0;
	for (z = 0; z < dim_z; z++) {
		for (x = 0; x < dim_x; x++) {
			loopindex = x + dim_x * y + dim_x * dim_y * z;
			flagField[loopindex] = 1;
		}
	}

	y = dim_y - 1;

	for (z = 0; z < dim_z; z++) {
			for (x = 0; x < dim_x; x++) {
				loopindex = x + y * dim_x + dim_x * dim_y * z;
				flagField[loopindex] = 1;
			}
		}

		x = 0;

		for (z = 0; z < dim_z; z++) {
			for (y = 0; y < dim_y; y++) {
				loopindex = x + y * dim_x + dim_x * dim_y * z;
				flagField[loopindex] = 1;
			}
		}

		x = dim_x - 1;

		for (z = 0; z < dim_z; z++) {
			for (y = 0; y < dim_y; y++) {
				loopindex = x + y * dim_x + dim_x * dim_y * z;
				flagField[loopindex] = 1;
			}
		}

if (atoi(argv[2]) == 1) {



/******** Tilted Plate Geometry ******/

    int **mask;
    mask = read_pgm("lbm_tilted_plate.vtk");

    for (z = 1; z < dim_z-1; z++) {
        for (y =0 ; y< dim_y -1 ; y++) {
            for (x = 1; x < dim_x-1; x++) {
                loopindex = x + y * dim_x + dim_x * dim_y * z;
                flagField[loopindex] = mask[dim_x -1 -x][dim_z -1 - z];
            }
        }

    }
    free_imatrix(mask,0, dim_x -2, 0, dim_z -2);

    /***** FREE SLIP XZ FACE** 7 ****/
    y = 0;

    for (z = 0; z < dim_z; z++) {
        for (x = 0; x < dim_x; x++) {
            loopindex = x + y * dim_x + dim_x * dim_y * z;
            flagField[loopindex] = 7;
        }
    }

    y = dim_y - 1;

    for (z = 0; z < dim_z; z++) {
        for (x = 0; x < dim_x; x++) {
            loopindex = x + y * dim_x + dim_x * dim_y * z;
            flagField[loopindex] = 7;
        }
    }

}

if (atoi(argv[2]) == 2) {

    /***** FREE SLIP YZ FACE ** 3 ****/

    x = 0;

    for (z = 0; z < dim_z; z++) {
        for (y = 0; y < dim_y; y++) {
            loopindex = x + y * dim_x + dim_x * dim_y * z;
            flagField[loopindex] = 3;
        }
    }

    x = dim_x - 1;

    for (z = 0; z < dim_z; z++) {
        for (y = 0; y < dim_y; y++) {
            loopindex = x + y * dim_x + dim_x * dim_y * z;
            flagField[loopindex] = 3;
        }
    }
}



if (atoi(argv[2]) == 3) {

/*************   the step geometry *****************************/
    for (int x = 0; x < dim_x / 2; x++) {
        for (int y = 0; y < dim_y; y++) {
            for (int z = 0; z < dim_x / 2; z++) {
                int loopindex = x + y * dim_x + dim_x * dim_y * z;
                flagField[loopindex] = 1;
            }
        }
    }
}


    /******** INITIALISE *********/

    for (int z = 0; z < dim_z; z++) {
        for (int y = 0; y < dim_y; y++) {
            for (int x = 0; x < dim_x; x++) {
                int loopindex = x + y * dim_x + dim_x * dim_y * z;

                int cell_index = Q * loopindex;

                for (int q = 0; q < Q; q++) {
                    collideField[cell_index + q] = LATTICEWEIGHTS[q];
                    streamField[cell_index + q] = LATTICEWEIGHTS[q];
                }
            }
        }
    }
}
